package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Departure {
    private int id;
    private Date departureDate;
    private int idPlanning;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "departure_date")
    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Basic
    @Column(name = "id_planning")
    public int getIdPlanning() {
        return idPlanning;
    }

    public void setIdPlanning(int idPlanning) {
        this.idPlanning = idPlanning;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Departure departure = (Departure) o;

        if (id != departure.id) return false;
        if (idPlanning != departure.idPlanning) return false;
        if (departureDate != null ? !departureDate.equals(departure.departureDate) : departure.departureDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (departureDate != null ? departureDate.hashCode() : 0);
        result = 31 * result + idPlanning;
        return result;
    }

    @Override
    public String toString() {
        return "Departure{" +
                "id=" + id +
                ", departureDate=" + departureDate +
                ", idPlanning=" + idPlanning +
                '}';
    }
}
