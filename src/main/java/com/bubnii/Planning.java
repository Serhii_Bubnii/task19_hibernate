package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Planning {
    private int id;
    private Timestamp departures;
    private short flightDuration;
    private int idPlane;
    private int idFlights;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "departures")
    public Timestamp getDepartures() {
        return departures;
    }

    public void setDepartures(Timestamp departures) {
        this.departures = departures;
    }

    @Basic
    @Column(name = "flight_duration")
    public short getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(short flightDuration) {
        this.flightDuration = flightDuration;
    }

    @Basic
    @Column(name = "id_plane")
    public int getIdPlane() {
        return idPlane;
    }

    public void setIdPlane(int idPlane) {
        this.idPlane = idPlane;
    }

    @Basic
    @Column(name = "id_flights")
    public int getIdFlights() {
        return idFlights;
    }

    public void setIdFlights(int idFlights) {
        this.idFlights = idFlights;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Planning planning = (Planning) o;

        if (id != planning.id) return false;
        if (flightDuration != planning.flightDuration) return false;
        if (idPlane != planning.idPlane) return false;
        if (idFlights != planning.idFlights) return false;
        if (departures != null ? !departures.equals(planning.departures) : planning.departures != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (departures != null ? departures.hashCode() : 0);
        result = 31 * result + (int) flightDuration;
        result = 31 * result + idPlane;
        result = 31 * result + idFlights;
        return result;
    }

    @Override
    public String toString() {
        return "Planning{" +
                "id=" + id +
                ", departures=" + departures +
                ", flightDuration=" + flightDuration +
                ", idPlane=" + idPlane +
                ", idFlights=" + idFlights +
                '}';
    }
}
