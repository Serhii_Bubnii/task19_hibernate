package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Ticket {
    private int id;
    private String codeTicket;
    private int idClient;
    private int idTicketPrice;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "code_ticket")
    public String getCodeTicket() {
        return codeTicket;
    }

    public void setCodeTicket(String codeTicket) {
        this.codeTicket = codeTicket;
    }

    @Basic
    @Column(name = "id_client")
    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    @Basic
    @Column(name = "id_ticket_price")
    public int getIdTicketPrice() {
        return idTicketPrice;
    }

    public void setIdTicketPrice(int idTicketPrice) {
        this.idTicketPrice = idTicketPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ticket ticket = (Ticket) o;

        if (id != ticket.id) return false;
        if (idClient != ticket.idClient) return false;
        if (idTicketPrice != ticket.idTicketPrice) return false;
        if (codeTicket != null ? !codeTicket.equals(ticket.codeTicket) : ticket.codeTicket != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codeTicket != null ? codeTicket.hashCode() : 0);
        result = 31 * result + idClient;
        result = 31 * result + idTicketPrice;
        return result;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", codeTicket='" + codeTicket + '\'' +
                ", idClient=" + idClient +
                ", idTicketPrice=" + idTicketPrice +
                '}';
    }
}
