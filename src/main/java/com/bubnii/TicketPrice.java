package com.bubnii;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ticket_price", schema = "bd_airport", catalog = "")
public class TicketPrice {
    private int id;
    private BigDecimal ticketPrice;
    private int idFlight;
    private int idPlanes;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ticket_price")
    public BigDecimal getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(BigDecimal ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    @Basic
    @Column(name = "id_flight")
    public int getIdFlight() {
        return idFlight;
    }

    public void setIdFlight(int idFlight) {
        this.idFlight = idFlight;
    }

    @Basic
    @Column(name = "id_plane")
    public int getIdPlanes() {
        return idPlanes;
    }

    public void setIdPlanes(int idPlanes) {
        this.idPlanes = idPlanes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TicketPrice that = (TicketPrice) o;

        if (id != that.id) return false;
        if (idFlight != that.idFlight) return false;
        if (idPlanes != that.idPlanes) return false;
        if (ticketPrice != null ? !ticketPrice.equals(that.ticketPrice) : that.ticketPrice != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (ticketPrice != null ? ticketPrice.hashCode() : 0);
        result = 31 * result + idFlight;
        result = 31 * result + idPlanes;
        return result;
    }

    @Override
    public String toString() {
        return "TicketPrice{" +
                "id=" + id +
                ", ticketPrice=" + ticketPrice +
                ", idFlight=" + idFlight +
                ", idPlanes=" + idPlanes +
                '}';
    }
}
