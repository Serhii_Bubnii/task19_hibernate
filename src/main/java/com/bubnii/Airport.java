package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Airport {
    private int id;
    private String nume;
    private String codeIata;
    private String codeIcao;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nume")
    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    @Basic
    @Column(name = "code_iata")
    public String getCodeIata() {
        return codeIata;
    }

    public void setCodeIata(String codeIata) {
        this.codeIata = codeIata;
    }

    @Basic
    @Column(name = "code_icao")
    public String getCodeIcao() {
        return codeIcao;
    }

    public void setCodeIcao(String codeIcao) {
        this.codeIcao = codeIcao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Airport airport = (Airport) o;

        if (id != airport.id) return false;
        if (nume != null ? !nume.equals(airport.nume) : airport.nume != null) return false;
        if (codeIata != null ? !codeIata.equals(airport.codeIata) : airport.codeIata != null) return false;
        if (codeIcao != null ? !codeIcao.equals(airport.codeIcao) : airport.codeIcao != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (nume != null ? nume.hashCode() : 0);
        result = 31 * result + (codeIata != null ? codeIata.hashCode() : 0);
        result = 31 * result + (codeIcao != null ? codeIcao.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "id=" + id +
                ", nume='" + nume + '\'' +
                ", codeIata='" + codeIata + '\'' +
                ", codeIcao='" + codeIcao + '\'' +
                '}';
    }
}
