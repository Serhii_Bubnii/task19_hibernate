package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Entity
public class Plane {
    private int id;
    private String type;
    private Date dateRelease;
    private short numberSeats;
    private String boardNumber;
    private String typeCabin;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "date_release")
    public Date getDateRelease() {
        return dateRelease;
    }

    public void setDateRelease(Date dateRelease) {
        this.dateRelease = dateRelease;
    }

    @Basic
    @Column(name = "number_seats")
    public short getNumberSeats() {
        return numberSeats;
    }

    public void setNumberSeats(short numberSeats) {
        this.numberSeats = numberSeats;
    }

    @Basic
    @Column(name = "board_number")
    public String getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(String boardNumber) {
        this.boardNumber = boardNumber;
    }

    @Basic
    @Column(name = "type_cabin")
    public String getTypeCabin() {
        return typeCabin;
    }

    public void setTypeCabin(String typeCabin) {
        this.typeCabin = typeCabin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Plane plane = (Plane) o;

        if (id != plane.id) return false;
        if (numberSeats != plane.numberSeats) return false;
        if (type != null ? !type.equals(plane.type) : plane.type != null) return false;
        if (dateRelease != null ? !dateRelease.equals(plane.dateRelease) : plane.dateRelease != null) return false;
        if (boardNumber != null ? !boardNumber.equals(plane.boardNumber) : plane.boardNumber != null) return false;
        if (typeCabin != null ? !typeCabin.equals(plane.typeCabin) : plane.typeCabin != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (dateRelease != null ? dateRelease.hashCode() : 0);
        result = 31 * result + (int) numberSeats;
        result = 31 * result + (boardNumber != null ? boardNumber.hashCode() : 0);
        result = 31 * result + (typeCabin != null ? typeCabin.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", dateRelease=" + dateRelease +
                ", numberSeats=" + numberSeats +
                ", boardNumber='" + boardNumber + '\'' +
                ", typeCabin='" + typeCabin + '\'' +
                '}';
    }
}
