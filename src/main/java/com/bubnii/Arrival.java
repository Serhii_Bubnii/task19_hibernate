package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Arrival {
    private int id;
    private Timestamp timeArrival;
    private int idDeparture;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "time_arrival")
    public Timestamp getTimeArrival() {
        return timeArrival;
    }

    public void setTimeArrival(Timestamp timeArrival) {
        this.timeArrival = timeArrival;
    }

    @Basic
    @Column(name = "id_departure")
    public int getIdDeparture() {
        return idDeparture;
    }

    public void setIdDeparture(int idDeparture) {
        this.idDeparture = idDeparture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Arrival arrival = (Arrival) o;

        if (id != arrival.id) return false;
        if (idDeparture != arrival.idDeparture) return false;
        if (timeArrival != null ? !timeArrival.equals(arrival.timeArrival) : arrival.timeArrival != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (timeArrival != null ? timeArrival.hashCode() : 0);
        result = 31 * result + idDeparture;
        return result;
    }

    @Override
    public String toString() {
        return "Arrival{" +
                "id=" + id +
                ", timeArrival=" + timeArrival +
                ", idDeparture=" + idDeparture +
                '}';
    }
}
