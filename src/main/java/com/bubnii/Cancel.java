package com.bubnii;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Cancel {
    private int id;
    private Timestamp timeCancellation;
    private String reason;
    private int idPlanning;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "time_cancellation")
    public Timestamp getTimeCancellation() {
        return timeCancellation;
    }

    public void setTimeCancellation(Timestamp timeCancellation) {
        this.timeCancellation = timeCancellation;
    }

    @Basic
    @Column(name = "reason")
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Basic
    @Column(name = "id_planning")
    public int getIdPlanning() {
        return idPlanning;
    }

    public void setIdPlanning(int idPlanning) {
        this.idPlanning = idPlanning;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cancel cancel = (Cancel) o;

        if (id != cancel.id) return false;
        if (idPlanning != cancel.idPlanning) return false;
        if (timeCancellation != null ? !timeCancellation.equals(cancel.timeCancellation) : cancel.timeCancellation != null)
            return false;
        if (reason != null ? !reason.equals(cancel.reason) : cancel.reason != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (timeCancellation != null ? timeCancellation.hashCode() : 0);
        result = 31 * result + (reason != null ? reason.hashCode() : 0);
        result = 31 * result + idPlanning;
        return result;
    }

    @Override
    public String toString() {
        return "Cancel{" +
                "id=" + id +
                ", timeCancellation=" + timeCancellation +
                ", reason='" + reason + '\'' +
                ", idPlanning=" + idPlanning +
                '}';
    }
}
